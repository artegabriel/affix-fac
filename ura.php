<?php

$codigo = $_POST['codigo'];

if (! $codigo) {
    exit(1);
}

$url = 'http://www.affixbeneficios.com.br/testing/cliente/ura';

$data = array('codigo' => $codigo);
$options = array(
    'http' => array(
        'method'  => 'POST',
        'header'    => "Content-type: application/x-www-form-urlencoded\r\n",
        'content'  => http_build_query($data)
    )
);

$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
?>