<style type="text/css">

/* =====================================================
	@company: Arte Digital Internet;
	@author: Gabriel Ramos;
	Display none necessário nos elemento select abaixo,
	além de #anexos para prevenir flashing de elementos
	escondidos pelo jquery.
   ===================================================== 
*/

@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900');

select[name*='menu']{
	display: none;
	margin: 10px 0;
}

body {
	font-family: 'Roboto', Arial, sans-serif;
	font-size: 13px;
	color: #373d45;
}

:focus{
	outline: none;
}

.menu,
.submenu {
	display: none;
	margin: 10px 0;
}

.fac {
	font-family: 'Roboto', Arial, sans-serif;
	width: 1200px;
	border: none;
}

.fac .opcao {
	display: inline-block;
	border-radius: 10px;
	margin-left: 10px;
	position: relative;
    left: -76px;
    top: -10px;
}

.fac .opcao label{
	display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 500;
    background: #00995D;
    padding: 5px 10px;
    color: #fff;
    border-radius: 22px;
    cursor: pointer;
}

.fac .opcao input{
	display: none;
}

.fac .bt_enviar,.bt_voltar{
	display: none;
	margin: 10px 4px;
	color: #FFF;
    background-color: #00995D;
    border: none;
    -moz-border-radius: 9px;
    -webkit-border-radius: 9px;
    border-radius: 9px;
    line-height: 13px;
    font-size: 1.2em;
    cursor: pointer;
}

.fac .bt_voltar{
	padding: 10px;
}

.bt_anexo p{	
    width: 50%;
    background: #bfe1f5;
    padding: 20px 10px;
    border: 2px solid #32a0e2;
    font-weight: 400 !important;
}

.sucesso {
	text-align: center;
	display: inline-block;
	padding: 5px 10px;
	border-radius: 10px;
	background-color: #B4F2B1;
	color: #156711;

	position: absolute;
    width: 50%;
    top: 170px;
    left: 17px;
    font-size: 1.2em;
}

#anexos {
	display: none;
}

#form-toggle {
	display: none;
}

#foto-toggle img {
	border-radius: 10px;
}

#foto-toggle {
	display: none;
    position: relative;
    float: right;
    top: 8px;
    margin-top: 60px;
}

#mensagem {
    display: none;
    background: url(/wp-content/themes/odin-master/images/msg-bg.png);
    padding: 30px 35px;
    width: 420px;
    height: 320px;
    font-family: Arial, Sans-Serif, Helvetica;
    color: #000;
    font-weight: normal;
    font-size: 1.1em;
 }

#mensagem.fixed {
    position: fixed;
    margin: auto;
    top: 25px;
    right: auto;
    float: none;
}

/*---------------------*/

.fac {
	/*width: 1200px;*/
	/*min-height: 320px;*/
	position: relative;
	list-style: none;
    background: url(/wp-content/themes/odin-master/images/ico_correio.jpg) no-repeat 19px 15px;
    width: 920px;
    border-color: rgb(238,238,238);
    border-width: 2px;
    -moz-border-radius: 37px;
    -webkit-border-radius: 37px;
    border-radius: 37px;
    border-style: solid;
    display: table;
    padding: 20px;
	transition: all 0.5s ease-in-out;
	-webkit-transition: all 0.5s ease-in-out;
	-o-transition: all 0.5s ease-in-out;
	-ms-transition: all 0.5s ease-in-out;
	-moz-transition: all 0.5s ease-in-out;
}

.fac input {
	padding: 10px;
}

.fac .bt_enviar,.bt_voltar{
    color: #FFF;
    background-color: #00995D;
    border: none;
    -moz-border-radius: 22px;
    -webkit-border-radius: 22px;
    border-radius: 22px;
    line-height: 12px;
    font-size: 13px;
    cursor: pointer;
    height: 30px;
    font-weight: bold;
}

.fac .bt_voltar{
	background-color: #afafaf;
}

.fac .bt_enviar:hover,.bt_voltar:hover, input[type="submit"]:hover{
	color: #FFF;
    background-color: #00995D;
    border: none;
}

.bt_anexo p{	
    width: 50%;
    background: #bfe1f5;
    padding: 20px 10px;
    border: 2px solid #32a0e2;
}

.fac .bt_voltar{
	padding: 10px;
}

.fac .campos {
	width: 50%;
    background-color: #eee;
    border: 1px solid #ccc;
    border-radius: 20px;
    padding: 5px 10px;
    font-size: 1.1em;
    resize: none;
    font-family: 'Roboto', Arial, sans-serif;
    font-weight: 400;
}

.contato-form div {
	float: none;
}

.fac select {
	font-family: 'Roboto', Arial, sans-serif;
	padding: 5px 10px;
    width: 50%;
    border-radius: 20px;
    background-color: #eee;
    border: 1px solid #ccc;
    font-size: 1.1em;
    color: #6a6a6a;
    font-weight: 400;
}

.fac .opcoes {
    margin-left: 68px;
    margin-top: 10px;
    line-height: 30px;
    position: relative;
    left: -61px;
    font-size: 14px;
    font-weight: 400;
    color: #373d45;
}

.entry-content {
	/*height: 950px;*/
	position: relative;
    top: 20px;
}

.entry-title {
	position: relative;
	top: -75px;
    left: 60px;
    float: none;
    margin-bottom: 0;
    font-size: 20px !important;
    font-weight: 400 !important;
}

.entry-frase {
	position: relative;
    left: 60px;
    top: 3px;
    font-size: 14px;
    font-weight: 500;
    color: #373d45;
    width: 45%;
    letter-spacing: 0px;
    margin-bottom: 15px;
}

input:required, textarea:required {
    background: #eee url(/wp-content/themes/odin-master/images/required.png) no-repeat 98% center;
}

input[type='file']:required {
	background: inherit;
}

#footer {
	margin-top: 18px !important;
}
#footer .site-info img {
	display: none;
}
#footer .site-info {
	height: 80px;
}

.mensagens h4 {
	font-weight: 400;
}

.resolva {
	position: relative;
    list-style: none;
    background: url(/wp-content/themes/odin-master/images/ico_pc.jpg) no-repeat 19px 15px;
    width: 920px;
    border-color: rgb(238,238,238);
    border-width: 2px;
    -moz-border-radius: 37px;
    -webkit-border-radius: 37px;
    border-radius: 37px;
    border-style: solid;
    display: table;
    padding: 20px;
}

.resolva h1 {
	color: #0c3177;
    font-weight: 200;
    font-size: 20px;
    position: relative;
    top: -24px;
    left: 60px;
}

.resolva p {
	font-size: 14px !important;
    position: relative;
    top: -28px;
    left: 60px;
    color: #373d45;
    font-family: 'Roboto', Arial, sans-serif;
    font-weight: 400 !important;
}

.resolva ul {
    position: relative;
    top: -11px;
    left: 20px;
}

.resolva ul li {
	list-style: none;
	display: inline-block;
	margin-right: 70px;
}

.contato-telefones li.col1 {
    margin-left: 52px !important;
    padding-right: 77px;
}

.contato-telefones li.col3 {
	width: 230px;
}

ul.contato-telefones {
	list-style: none;
    background: url(/wp-content/themes/odin-master/images/ico_tel.jpg) no-repeat 20px 15px !important;
    width: 920px;
    height: 160px !important;
    border-color: rgb(238,238,238);
    border-width: 2px;
    -moz-border-radius: 37px;
    -webkit-border-radius: 37px;
    border-radius: 37px;
    border-style: solid;
}

.telefones {
	display: none;
	position: relative;
    top: -13px;
}

.contato-telefones li {
	margin-right: 45px !important;
}

.tel-choose { 
	margin-left: 70px;
}

.tel-choose select{
	width: 100%;
	font-family: 'Roboto', Arial, sans-serif;
	padding: 5px 10px;
    border-radius: 20px;
    background-color: #eee;
    border: 1px solid #ccc;
    font-size: 1.1em;
    color: #6a6a6a;
    font-weight: 400;
}
.tel-choose h3 {
	font-size: 17px;
    margin-top: 10px;
    font-weight: 700;
}
.tel-all {

}

/* ajustes box mensagem*/

.box-container {
    width: 500px;
    height: 200px;
    margin: auto;
    position: absolute;
    margin-top: 14px;
    z-index: 3;
    margin-left: 480px;
}

.box-relative {
    position: relative;
    width: 500px;
    height: 320px;
}
#mensagem {
    position: relative;
}

form.fac {
    position: relative;
    z-index: 2;
}
/* fim ajustes box mensagem*/

</style>


<?php
/**
 * Template Name: fac
 *
 * The template for displaying pages with sidebar.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header();
?>


<div id="primary">
		<div id="content" class="site-content" role="main">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <!-- botao voltar -->
    <button class="voltar" onclick="goBack()"><img src="<?php echo get_template_directory_uri(); ?>/images/voltar.png" /></button><!-- breadcrumbs -->
    <div class="breadcrumbs"><?php wp_custom_breadcrumbs(); ?></div>
  
  	<div class="entry-content">
		<?php
			the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'odin' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>


<div class="resolva">
<h1>Resolva Online</h1>
<p>Quer Soluções simples e rápidas? Confira os serviços disponíveis da área Sou cliente Affix.</p>
<ul>
<li><a href="http://www.affixbeneficios.com.br/cliente/boletos" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/images/btn_boleto.png"></a></li>
<li><a href="http://www.affixbeneficios.com.br/cliente/" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/images/btn_atual.jpg"></a></li>
<li><a href="http://www.affixbeneficios.com.br/cliente/" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/images/btn_cart.jpg"></a></li>
<li><a href="http://www.affixbeneficios.com.br/cliente/" target="_blank"><img src="<?php echo get_bloginfo('template_directory');?>/images/btn_extr.jpg"></a></li>
</ul>
</div>

<br />

<!-- INICIO BOX MENSAGEM -->
<div class="box-container">

    <div class="box-relative">
	<div id="mensagem"></div>
    </div>
    
</div>
<!-- FIM BOX MENSAGEM -->

<form action="<?php echo get_template_directory_uri(); ?>/processar.php" method="post" class="fac" enctype="multipart/form-data">

	<?= (isset($_GET["envio"]) and $_GET["envio"] == "ok") ? "<div class='sucesso'>Sua mensagem foi enviada com sucesso!</div>" : ""?>
	<h1 class="entry-frase">Não encontou o que procura? Converse com a gente:</h1>
	<h1 class="entry-title"><?php the_title(); ?></h1>
	<p class="opcoes">
		Escolha uma das opções abaixo:
		<div class="opcao">
			<input type="radio" id="acs-sim" name="tipo-acesso" value="sim" class="tipo-acesso" required> <label for="acs-sim"> Sou cliente</label>
		</div>

		<div class="opcao">
			<input type="radio" id="acs-nao" name="tipo-acesso" value="nao" class="tipo-acesso" required> <label for="acs-nao"> Quero ser cliente</label>
		</div>

		<div class="opcao">
			<input type="radio" id="acs-corretor" name="tipo-acesso" value="corretor" class="tipo-acesso" required> <label for="acs-corretor"> Sou corretor</label>
		</div>

		<!-- <div class="opcao">
			<input type="radio" id="acs-vendedor" name="tipo-acesso" value="vendedor" class="tipo-acesso" required> <label for="acs-vendedor"> Quero ser vendedor</label>
		</div> -->
	</p>

	<div id="msg-toggle"></div>

	<!-- cliente-sim-->
	<select id="cliente-sim" name="cliente-sim" class="menu">
		<option selected="selected">Escolha uma das opções abaixo:</option>
		<option value="atualizacao-dados">Atualização de dados cadastrais</option>
		<option value="pagamentos">Pagamentos</option>
		<option value="segundavia-carteirinha">2ª Via de carteirinha</option>
		<option value="alteracao-acomodacao">Alteração de acomodação</option>
		<option value="cancelamento-plano">Cancelamento do plano de saúde</option>
		<option value="carta-permanencia">Carta de Permanência</option>
		<option value="inclusao-recemnascido">Inclusão de recém-nascido e recém casado</option>
		<option value="carta-autorizacao">Carta de Autorização - Unimed Rio Grande do Norte e Unimed Norte/Nordeste</option>
		<option value="elogio">Elogio</option>
		<option value="reclamacao">Reclamação</option>
	</select> 

	<!-- cliente-sim-atualizacao-dados -->
	<select id="cliente-sim-atualizacao-dados" name="cliente-sim-atualizacao-dados" class="submenu">
		<option selected="selected">Escolha uma das opções abaixo:</option>
		<option value="endereco">Endereço, telefone e e-mail</option>
		<option value="rg">Nome, RG, CPF e data de nascimento</option>
	</select>

	<!-- cliente-sim-pagamentos -->
	<select id="cliente-sim-pagamentos" name="cliente-sim-pagamentos" class="submenu">
		<option selected="selected">Escolha uma das opções abaixo:</option>
		<option value="segundavia-boleto">2ª via de boleto</option>
		<option value="extrato-demonstrativo">Extrato de Coparticipação ou Demonstrativo de Pagamento</option>
		<option value="duplicidade-pagamento">Pagamento efetuado em duplicidade</option>
		<option value="confirmar-pagamento">Confirmar pagamento realizado</option>
		<option value="negociacao-debitos">Negociação de débitos</option>
	</select>

	<!-- cliente-sim-reclamacao -->
	<select id="cliente-sim-reclamacao" name="cliente-sim-reclamacao" class="submenu">
		<option selected="selected">Escolha uma das opções abaixo:</option>
		<option value="rede-credenciada">Rede credenciada</option>
		<option value="falta-informacao">Falta de informação na contratação</option>
		<option value="recusa-atendimento">Recusa de atendimento</option>
		<option value="reajuste-valor">Reajuste de valor</option>		
	</select>
	<!-- Fim Cliente -Sim -->

	<!-- cliente-nao -->
	<select id="cliente-nao" name="cliente-nao" class="menu">
		<option selected="selected">Escolha uma das opções abaixo:</option>
		<option value="contratar-plano">Quero contratar um plano de saúde</option>
	</select>
	<!-- Fim Cliente -Não -->

	<!-- corretor -->
	<select id="corretor" name="corretor" class="menu">
		<option selected="selected">Escolha uma das opções abaixo:</option>
		<option value="duvida-premiacao">Dúvida referente premiação</option>
		<option value="cartao-premiacao">Cartão Premiação</option>
		<option value="informacao-campanha">Informação de campanha</option>
		<option value="informacao-beneficiario">Informação do beneficiário</option>
	</select>

	<!-- corretor-cartao-premiacao -->
	<select id="corretor-cartao-premiacao" name="corretor-cartao-premiacao" class="submenu">
		<option selected="selected">Escolha uma das opções abaixo:</option>
		<option value="senha">Senha</option>
		<option value="bloqueio">Bloqueio</option>
		<option value="segundavia-cartao">2ª Via de cartão</option>
	</select>

	<!-- corretor-informacao-campanha -->
	<select id="corretor-informacao-campanha" name="corretor-informacao-campanha" class="submenu">
		<option selected="selected">Escolha um dos seguintes estados:</option>
		<option value="ic-1">Acre</option>
		<option value="ic-2">Alagoas</option>
		<option value="ic-3">Amapá</option>
		<option value="ic-4">Amazonas</option>
		<option value="ic-5">Bahia</option>
		<option value="ic-6">Ceará</option>
		<option value="ic-7">Distrito Federal</option>
		<option value="ic-8">Espírito Santo</option>
		<option value="ic-9">Goiás</option>
		<option value="ic-10">Maranhão</option>
		<option value="ic-11">Mato Grosso</option>
		<option value="ic-12">Mato Grosso do Sul</option>
		<option value="ic-13">Minas Gerais</option>
		<option value="ic-14">Pará</option>
		<option value="ic-15">Paraíba</option>
		<option value="ic-16">Paraná</option>
		<option value="ic-17">Pernambuco</option>
		<option value="ic-18">Piauí</option>
		<option value="ic-19">Rio de Janeiro</option>
		<option value="ic-20">Rio Grande do Norte</option>
		<option value="ic-21">Rio Grande do Sul</option>
		<option value="ic-22">Rondônia</option>
		<option value="ic-23">Roraima</option>
		<option value="ic-24">Santa Catarina</option>
		<option value="ic-25">São Paulo</option>
		<option value="ic-26">Sergipe</option>
		<option value="ic-27">Tocantins</option>
	</select>
	<!-- Fim Corretor -->

	<!-- vendedor-->
	<select id="cliente-vendedor" name="cliente-vendedor" class="menu">
		<option selected="selected">Escolha uma das opções abaixo:</option>
		<option value="vendedor">Quero ser um vendedor</option>
	</select>
	<!-- Fim Vendedor -->

	<!--<div id="foto-toggle"><img src=""></div>-->

	<div id="form-toggle"></div>
	
	<div id="anexos" class="bt_anexo"></div>

	<input type="submit" name="submit" value="Enviar" class="bt_enviar">
	<button type="reset" onclick="resetStats();" class="bt_voltar">Voltar</button>
</form>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
   window.jQuery || document.write("<script type='text/javascript' src='jquery-3.2.1.min.js'/><\/script>");
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
<script type="text/javascript">

function resetStats() {

	$(".sucesso").hide();
	$("select").hide();
	$("#anexos").hide();
	$("#mensagem").hide();
	$("#foto-toggle").hide();
	$("#msg-toggle").hide();
	$("#form-toggle").hide();
	$(".bt_voltar").hide();
	$(".bt_enviar").hide();
	$(".telefones").hide();
}

//valida o CPF digitado
function validarCPF(Objcpf){
        var cpf = Objcpf.value;
        exp = /\.|\-/g
        cpf = cpf.toString().replace( exp, "" ); 
        var digitoDigitado = eval(cpf.charAt(9)+cpf.charAt(10));
        var soma1=0, soma2=0;
        var vlr =11;

        for(i=0;i<9;i++){
                soma1+=eval(cpf.charAt(i)*(vlr-1));
                soma2+=eval(cpf.charAt(i)*vlr);
                vlr--;
        }       
        soma1 = (((soma1*10)%11)==10 ? 0:((soma1*10)%11));
        soma2=(((soma2+(2*soma1))*10)%11);

        var digitoGerado=(soma1*10)+soma2;
        if(digitoGerado!=digitoDigitado) {
        	$(this).addClass("invalid-cpf");
        	return false;
        }
                         
}

$(document).ready(function() {

	$("select[name^='c']").on("change", function() {

		var uraMap = {
			"endereco": 15,
			"rg": 16,
			"segundavia-boleto": 17,
			"extrato-demonstrativo": 18,
			"duplicidade-pagamento": 19,
			"confirmar-pagamento": 20,
			"negociacao-debitos": 21,
			"segundavia-carteirinha": 22,
			"alteracao-acomodacao": 23,
			"cancelamento-plano": 24,
			"carta-permanencia": 25,
			"inclusao-recemnascido": 26,
			"carta-autorizacao": 27,
			// todo: por codigo pra opcao elogio
			"rede-credenciada": 28,
			"falta-informacao": 29,
			"recusa-atendimento": 30,
			"reajuste-valor": 31,
			"contratar-plano": 32,
			"duvida-premiacao": 33,
			"senha": 34,
			"bloqueio": 35,
			"segundavia-cartao": 36,
			"ic-1": 37,
			"ic-2": 37,
			"ic-3": 37,
			"ic-4": 37,
			"ic-5": 37,
			"ic-6": 37,
			"ic-7": 37,
			"ic-8": 37,
			"ic-9": 37,
			"ic-10": 37,
			"ic-11": 37,
			"ic-12": 37,
			"ic-13": 37,
			"ic-14": 37,
			"ic-15": 37,
			"ic-16": 37,
			"ic-17": 37,
			"ic-18": 37,
			"ic-19": 37,
			"ic-20": 37,
			"ic-21": 37,
			"ic-22": 37,
			"ic-23": 37,
			"ic-24": 37,
			"ic-25": 37,
			"ic-26": 37,
			"ic-27": 37,
			"informacao-beneficiario": 38
			// todo: por o codigo de quero ser vendedor
			// "vendedor": codigo
		}

		if(uraMap[$(this).val()] !== undefined) {
			alert("Código: "+uraMap[$(this).val()]);
		}		

		if(uraMap[$(this).val()]) {

			var ura = uraMap[$(this).val()];

			$.ajax({
				type: "POST",
				url: "http://www.affixbeneficios.com.br/wp-content/themes/odin-master/ura.php",
				data: { codigo: ura }
			});
		}
	
	});

	var formPadrao = "<p><input type='text' name='nome' placeholder='Nome' maxlength=50 class='campos' required></p><p><input type='text' name='cpf' placeholder='CPF Titular' class='campos' required></p><p><input type='text' name='telefone' placeholder='Telefone' class='campos' required></p><p><input type='email' name='email' placeholder='E-mail: exemplo@email.com.br' class='campos' required></p><p><input type='hidden' name='form-type' value='padrao'></p>";

	var formContratar = "<p><input type='text' name='nome' placeholder='Nome completo' maxlength=50 class='campos' required></p><p><input type='text' name='telfixo' placeholder='Telefone fixo' class='campos' required></p><p><input type='text' name='celular' placeholder='Celular' class='campos' required></p><p><input type='text' name='cidade' placeholder='Cidade' class='campos' required></p><p><select id='cp-opcao' name='cp-opcao' required><option selected='selected'>Escolha um dos seguintes estados:</option><option value='cp-1'>Acre</option><option value='cp-2'>Alagoas</option><option value='cp-3'>Amapá</option><option value='cp-4'>Amazonas</option><option value='cp-5'>Bahia</option><option value='cp-6'>Ceará</option><option value='cp-7'>Distrito Federal</option><option value='cp-8'>Espírito Santo</option><option value='cp-9'>Goiás</option><option value='cp-10'>Maranhão</option><option value='cp-11'>Mato Grosso</option><option value='cp-12'>Mato Grosso do Sul</option><option value='cp-13'>Minas Gerais</option><option value='cp-14'>Pará</option><option value='cp-15'>Paraíba</option><option value='cp-16'>Paraná</option><option value='cp-17'>Pernambuco</option><option value='cp-18'>Piauí</option><option value='cp-19'>Rio de Janeiro</option><option value='cp-20'>Rio Grande do Norte</option><option value='cp-21'>Rio Grande do Sul</option><option value='cp-22'>Rondônia</option><option value='cp-23'>Roraima</option><option value='cp-24'>Santa Catarina</option><option value='cp-25'>São Paulo</option><option value='cp-26'>Sergipe</option><option value='cp-27'>Tocantins</option></select></p><p><input type='email' name='email' placeholder='E-mail: exemplo@email.com.br' class='campos' required></p><p><input type='hidden' name='form-type' value='contratar'></p>";

	// todo: ajustar conforme necessário, ou reutilizar o de cima se possível
	var formVendedor = "<p><input type='text' name='nome' placeholder='Nome completo' maxlength=50 class='campos' required></p><p><input type='text' name='telfixo' placeholder='Telefone fixo' class='campos'></p><p><input type='text' name='celular' placeholder='Celular' class='campos' required></p><p><input type='text' name='cidade' placeholder='Cidade' class='campos' required></p><p><select id='cp-opcao' name='cp-opcao' required><option selected='selected'>Escolha um dos seguintes estados:</option><option value='cp-1'>Acre</option><option value='cp-2'>Alagoas</option><option value='cp-3'>Amapá</option><option value='cp-4'>Amazonas</option><option value='cp-5'>Bahia</option><option value='cp-6'>Ceará</option><option value='cp-7'>Distrito Federal</option><option value='cp-8'>Espírito Santo</option><option value='cp-9'>Goiás</option><option value='cp-10'>Maranhão</option><option value='cp-11'>Mato Grosso</option><option value='cp-12'>Mato Grosso do Sul</option><option value='cp-13'>Minas Gerais</option><option value='cp-14'>Pará</option><option value='cp-15'>Paraíba</option><option value='cp-16'>Paraná</option><option value='cp-17'>Pernambuco</option><option value='cp-18'>Piauí</option><option value='cp-19'>Rio de Janeiro</option><option value='cp-20'>Rio Grande do Norte</option><option value='cp-21'>Rio Grande do Sul</option><option value='cp-22'>Rondônia</option><option value='cp-23'>Roraima</option><option value='cp-24'>Santa Catarina</option><option value='cp-25'>São Paulo</option><option value='cp-26'>Sergipe</option><option value='cp-27'>Tocantins</option></select></p><p><input type='email' name='email' placeholder='E-mail: exemplo@email.com.br' class='campos' required></p><p><input type='hidden' name='form-type' value='vendedor'></p>";

	var formCorretorCPF = "<p><input type='text' name='nome' placeholder='Nome completo do corretor/vendedor' maxlength=50 class='campos' required></p><p><input type='text' name='cpf' placeholder='CPF do corretor/vendedor' class='campos' required></p><p><input type='text' name='telefone' placeholder='Telefone do corretor/vendedor' class='campos' required></p><p><input type='email' name='email' placeholder='E-mail do corretor/vendedor: exemplo@email.com.br' class='campos' required></p><p><input type='text' name='corretora' placeholder='Nome da corretora' class='campos' required></p><p><input type='text' name='cpf-cliente' placeholder='CPF do cliente' class='campos' required></p><p><input type='hidden' name='form-type' value='corretorcpf'></p>";

	var formCorretor = "<p><input type='text' name='nome' placeholder='Nome completo do corretor/vendedor' maxlength=50 class='campos' required></p><p><input type='text' name='cpf' placeholder='CPF do corretor/vendedor' class='campos' required></p><p><input type='text' name='telefone' placeholder='Telefone do corretor/vendedor' class='campos' required></p><p><input type='email' name='email' placeholder='E-mail do corretor/vendedor: exemplo@email.com.br' class='campos' required></p><p><input type='text' name='corretora' placeholder='Nome da corretora' class='campos' required></p><p><input type='hidden' name='form-type' value='corretor'></p>";

	var textAreaPadrao = "<p><textarea name='comentarios' rows='5' cols='35' placeholder='Comentários' class='campos' required></textarea></p>";

	var textAreaCorretor = "<p><textarea name='msg-cliente' rows='5' cols='35' placeholder='Mensagem do cliente' class='campos' required></textarea></p>";

	var textAreaInfoCampanha = "<p><textarea name='msg-cliente' rows='5' cols='35' placeholder='Mensagem' class='campos' required></textarea></p>";

	var msgDados = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Para essa atualização acesse a área logada da Affix, <a href='http://www.affixbeneficios.com.br/cliente/'>clicando aqui</a>. Caso tenha dúvidas, ligue para nossa Central utlizando os telefones abaixo.</div>";

	var msgRG = "<div class='mensagens'>Para alterar RG, CPF ou responsável financeiro, anexar documentação obrigatória:<ul><li>Nome: Cópia do Documento de RG</li><li>Data de nascimento: Cópia do Documento de RG</li><li>RG: Cópia do Documento</li><li>CPF: Cópia do documento</li></div>";

	var msgViaBoleto = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Para essa solicitação acesse a área logada da Affix, <a href='http://www.affixbeneficios.com.br/cliente/'>clicando aqui</a>. Caso tenha dúvidas, ligue para nossa Central utilizando os telefones abaixo.</div>";

	var msgDemoPag = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Para essa solicitação acesse a área logada da Affix, <a href='http://www.affixbeneficios.com.br/cliente/'>clicando aqui</a>. Caso tenha dúvidas, ligue para nossa Central utilizando os telefones abaixo.</div>";

	var msgDuplPag = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Para pagamento realizado em duplicidade, anexar documentação obrigatória: <b>Comprovante de Pagamento.</b></div>";

	var msgConfPag = "<div class='mensagens'><h4>Prezado beneficiário,</h4>A confirmação do pagamento realizado ocorre em até <b>03 dias úteis</b> após a data em que foi realizado, caso queira confirmar, ligue em nossa Central utlizando os telefones abaixo.</div>";

	var msgNegPag = "<div class='mensagens'><h4>Prezado beneficiário,</h4>para essa solicitação ligue para nossa Central utilizando os telefones abaixo.</div>";

	var msgSouCliente =  "<div class='mensagens'><h4>Prezado cliente</h4>Siga as opções abaixo para entrar em contato com a nossa equipe.</div>";

	var msgNaoSouCliente = "<div class='mensagens'><h4>Será um grande prazer atender você!</h4>Siga as opções abaixo para entrar em contato com nossa equipe comercial.</div>";

	var msgSouCorretor = "<div class='mensagens'><h4>Prezado corretor</h4>Siga as opções abaixo para entrar em contato com nossa equipe de suporte.</div>";

	// todo: ajustar conforme necessário
	var msgVendedor = "<div class='mensagens'><h4>Será um grande prazer atender você!</h4>Siga as opções abaixo para entrar em contato com nossa equipe comercial.</div>";

	var msgViaCartao = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Para essa solicitação acesse a área logada da Affix, <a href='http://www.affixbeneficios.com.br/cliente/'>clicando aqui</a>. Caso tenha dúvidas, ligue para nossa Central utlizando os telefones abaixo.</div>";

	var msgAltAcomodacao = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Entraremos em contato por telefone para informar os procedimentos necessários para alteração de sua acomodação atual.</div>";

	var msgCancelamento = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Para essa solicitação acesse a área logada da Affix, <a href='http://www.affixbeneficios.com.br/cliente/'>clicando aqui</a>. Caso tenha dúvidas, ligue para nossa Central utlizando os telefones abaixo.</div>";

	var msgCartaPerm = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Favor informar <b>Nome Completo</b> e <b>CPF do titular</b> para emissão da carta.</div>";

	var msgInclRecemNascido = "<div class='mensagens'><h4>Prezado beneficiário.</h4>Para inclusão de recém nascido ou recém casado em seu plano de saúde, anexar as documentações obrigatórias: <br> Recém-nascido: <b>Certidão de Nascimento</b>, <b>cartão do SUS e CPF do recém nascido</b>, <b>e a carta de próprio punho do titular do plano solicitando a inclusão.</b> <br> Recém casado: <b>Certidão de Casamento</b>, <b>cartão do SUS e CPF do cônjuge<b> <b>e a carta de próprio punho do titular do plano solicitando a inclusão.</b></div>";

	var msgCartaAut = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Favor informar o motivo da solicitação.</div>";

	var msgElogio = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Agradecemos seu contato.</div>";

	var msgRedeCred = "<div class='mensagens'><h4>Prezado beneficiário,</h4> para consultar a rede credenciada acesse o site da Operadora ou clique aqui para obter o telefone.</div>";

	var msgReclamacao = "<div class='mensagens'><h4>Prezado beneficiário,</h4>Agradecemos seu contato e entraremos em contato, por favor confirme em nosso site se seus telefones e e-mail estão atualizados na área logada da Affix, opção <b>Meus Dados</b>.</div>";

	var msgContratar = "<div class='mensagens'><h4>Obrigada por procurar a Affix,</h4>Em breve um de nossos corretores parceiros entrarão em contato para lhe apresentar nossas opções de contratação.</div>";

	// todo: ajustar conforme necessário
	var msgSerVendedor = "<div class='mensagens'><h4>Obrigada por procurar a Affix,</h4>Em breve um de nossos corretores parceiros entrarão em contato para lhe apresentar nossas opções de contratação.</div>";

	var msgPrazo = "<div class='mensagens'><h4>Caro parceiro,</h4>O prazo de resposta para essa dúvida é de <b>72 horas</b>. Agradecemos a parceria.</div>";

	var inputAnexos = "<span id='input-anexos'>Anexos: <input type='file' name='arquivos[]' id='arquivos' accept='.doc, .docx, application/pdf, .jpg, .png, .gif' multiple='multiple' required><p>Tamanho: 2 MB por arquivo. (máximo 20 MB no total).<br> Enviar apenas arquivos de texto (.doc, .docx, .pdf, .jpg, .png, e .gif)</p></span>";

	$(".menu").attr("disabled", true);
	$(".submenu").attr("disabled", true);
	$("#form-toggle").attr("disabled", true);
	$("#form-toggle input").attr("disabled", true);

	$(".tipo-acesso").on("change", function() {	
		
		$(".sucesso").hide();
		$(".menu").attr("disabled", true);
		$("#form-toggle input").attr("disabled", true);
		$(".menu").hide();
		$(".submenu").hide();
		$(".bt_enviar").hide();
		$(".bt_voltar").hide();

		$("#anexos").hide();
		$("#foto-toggle").hide();	
		$("#mensagem").hide();
		$("#msg-toggle").hide();
		$("#form-toggle").hide();				

		/* ==========================================================
			@company: Arte Digital Internet;
			@author: Gabriel Ramos;
			Os selects de menu e submenu utilizam o metodo css() para 
			mudar o valor do display devido ao bug em que o jquery 
			ao utilizar o método show() mostra o elemento com display 
			inline-block e não block como deveria ser.
			=========================================================
		*/

		var tipoAcesso = $(this).val();

		switch(tipoAcesso) {
			case "sim":
				$("#cliente-sim").css("display", "block");
				$("#cliente-sim").trigger("change");
				$("#cliente-sim").attr("disabled", false);
				$("#msg-toggle").show().html(msgSouCliente);
				break;

			case "nao":
				$("#cliente-nao").css("display", "block");
				$("#cliente-nao").trigger("change");
				$("#cliente-nao").attr("disabled", false);
				$("#msg-toggle").show().html(msgNaoSouCliente);
				break;

			case "corretor":
				$("#corretor").css("display", "block");
				$("#corretor").trigger("change");
				$("#corretor").attr("disabled", false);
				$("#msg-toggle").show().html(msgSouCorretor);
				break;

			case "vendedor":
				$("#cliente-vendedor").css("display", "block");
				$("#cliente-vendedor").trigger("change");
				$("#cliente-vendedor").attr("disabled", false);
				$("#msg-toggle").show().html(msgVendedor);
				break;
		}
	});

	$(".menu").on("change", function() {

		// Alternando entre menus retorna à primeira
		// opção do menu como padrão.
		$(this).siblings().prop('selectedIndex', function(index) {

	        var $selectedOption = $(this).find("option[selected='selected']");  

	        if($selectedOption.length)
	            return $selectedOption.index();

	        return 0;
    	});

		$(".sucesso").hide();
		$(".submenu").attr("disabled", true);
		$("#form-toggle input").attr("disabled", true);
		$(".submenu").hide();
		$(".bt_enviar").hide();
		$(".bt_voltar").hide();
		$("#anexos").hide();
		$("#input-anexos").remove();
		$("#foto-toggle").hide();
		$("#msg-toggle").hide();
		$("#mensagem").hide();
		$("#form-toggle").hide();

		$(".telefones").hide();

		$(".alagoas").hide();
		$(".amazonas").hide();
		$(".bahia").hide();
		$(".ceara").hide();
		$(".espiritosanto").hide();
		$(".maranhao").hide();
		$(".minasgerais").hide();
		$(".para").hide();
		$(".paraiba").hide();
		$(".pernambuco").hide();
		$(".piaui").hide();
		$(".riograndedonorte").hide();
		$(".saopaulo").hide();
		$(".sergipe").hide();


		var menu = $(this).val();

		switch(menu) {
			case "atualizacao-dados":
				$("#cliente-sim-atualizacao-dados").css("display", "block");
				$("#cliente-sim-atualizacao-dados").trigger("change");
				$("#cliente-sim-atualizacao-dados").attr("disabled", false);
				$("#foto-toggle").css("margin-top", "70px");
				$(".fac").css("min-height", "330px");
				break;

			case "segundavia-carteirinha":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto1.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgViaCartao);
				$(".telefones").show();
				$("#foto-toggle").css("margin-top", "110px");
				$(".fac").css("min-height", "330px");
				break;

			case "alteracao-acomodacao":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto1.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgAltAcomodacao);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#foto-toggle").css("margin-top", "50px");
				break;

			case "cancelamento-plano":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto1.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgCancelamento);
				$(".fac").css("min-height", "330px");
				$(".telefones").show();
				$("#foto-toggle").css("margin-top", "110px");
				break;

			case "carta-permanencia":
				$("#mensagem").show().html(msgCartaPerm);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				break;

			case "inclusao-recemnascido":
				$("#anexos").show();
				$("#anexos").append(inputAnexos);
				$("#mensagem").show().html(msgInclRecemNascido);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				break;

			case "carta-autorizacao":
				$("#mensagem").show().html(msgCartaAut);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				break;

			case "elogio":
				$("#mensagem").show().html(msgElogio);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				break;

			case "pagamentos":
				$("#cliente-sim-pagamentos").css("display", "block");
				$("#cliente-sim-pagamentos").trigger("change");
				$("#cliente-sim-pagamentos").attr("disabled", false);
				break;

			case "reclamacao":
				$("#cliente-sim-reclamacao").css("display", "block");
				$("#cliente-sim-reclamacao").trigger("change");
				$("#cliente-sim-reclamacao").attr("disabled", false);
				break;

			case "contratar-plano":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto3.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgContratar);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formContratar);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#foto-toggle").css("margin-top", "65px");
				break;

			case "duvida-premiacao":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto3.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgPrazo);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formCorretorCPF+textAreaCorretor);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#foto-toggle").css("margin-top", "25px");
				break;

			case "cartao-premiacao":
				$("#corretor-cartao-premiacao").css("display", "block");
				$("#corretor-cartao-premiacao").trigger("change");
				$("#corretor-cartao-premiacao").attr("disabled", false);
				$("#foto-toggle").css("margin-top", "-10px");
				break;

			case "informacao-campanha":
				$("#corretor-informacao-campanha").css("display", "block");
				$("#corretor-informacao-campanha").trigger("change");
				$("#corretor-informacao-campanha").attr("disabled", false);
				break;

			case "informacao-beneficiario":
				$("#mensagem").show().html(msgPrazo);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formCorretorCPF+textAreaCorretor);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				break;

			case "vendedor":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto3.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgSerVendedor);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formVendedor);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#foto-toggle").css("margin-top", "65px");
				break;

			case "alagoas":
				$(".alagoas").show();
				break;
		}

		$("input[name='cpf']").mask("000.000.000-00");
		$("input[name='cpf-cliente']").mask("000.000.000-00");
		$("input[name*='tel']").mask("(99) 99999-9999");
		$("input[name='celular']").mask("(99) 99999-9999");
	});

	$(".submenu").on("change", function() {
		
		$("#form-toggle input").attr("disabled", true);
		$(".sucesso").hide();	
		$(".bt_enviar").hide();
		$(".bt_voltar").hide();
		$("#anexos").hide();
		$("#input-anexos").remove();
		$("#foto-toggle").hide();
		$("#mensagem").hide();
		$("#form-toggle").hide();
		$(".telefones").hide();	
		$("#telefones-regioes").hide();	

		var submenu = $(this).val();

		switch(submenu) {
			case "endereco":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto1.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgDados);
				$(".fac").css("min-height", "330px");
				$(".telefones").show();
				$("#telefones-regioes").show();
				break;

			case "rg":
				$("#anexos").show();
				$("#anexos").append(inputAnexos);
				$("#mensagem").show().html(msgRG);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#telefones-regioes").show();
				break;

			case "segundavia-boleto":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto1.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgViaBoleto);
				$(".fac").css("min-height", "330px");
				$(".telefones").show();
				$("#telefones-regioes").show();
				$("#foto-toggle").css("margin-top", "35px");
				break;

			case "extrato-demonstrativo":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto1.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgDemoPag);
				$(".fac").css("min-height", "330px");
				$(".telefones").show();
				$("#telefones-regioes").show();
				$("#foto-toggle").css("margin-top", "55px");
				break;

			case "duplicidade-pagamento":
				$("#anexos").show();
				$("#anexos").append(inputAnexos);
				$("#mensagem").show().html(msgDuplPag);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#telefones-regioes").show();
				break;

			case "confirmar-pagamento":
				$("#anexos").show();
				$("#anexos").append(inputAnexos);
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto1.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgConfPag);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#telefones-regioes").show();
				$("#foto-toggle").css("margin-top", "0px");
				break;

			case "negociacao-debitos":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto1.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgNegPag);
				$(".fac").css("min-height", "330px");
				$(".telefones").show();
				$("#telefones-regioes").show();
				$("#foto-toggle").css("margin-top", "15px");
				break;

			case "rede-credenciada":
				$("#mensagem").show().html(msgRedeCred);
				$(".fac").css("min-height", "330px");
				$(".telefones").show();
				$("#telefones-regioes").show();
				break;

			case "falta-informacao":
			case "recusa-atendimento":
			case "reajuste-valor":
				$("#mensagem").show().html(msgReclamacao);
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formPadrao+textAreaPadrao);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#telefones-regioes").show();
				break;

			case "senha":
			case "bloqueio":
			case "segundavia-cartao":
				$("#foto-toggle img").attr("src", "<?php echo get_template_directory_uri(); ?>/imagens/foto3.jpg");
				$("#foto-toggle").show();
				$("#mensagem").show().html(msgPrazo);	
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formCorretor+textAreaCorretor);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#telefones-regioes").show();
				break;

			case "ic-1":
			case "ic-2":
			case "ic-3":
			case "ic-4":
			case "ic-5":
			case "ic-6":
			case "ic-7":
			case "ic-8":
			case "ic-9":
			case "ic-10":
			case "ic-11":
			case "ic-12":
			case "ic-13":
			case "ic-14":
			case "ic-15":
			case "ic-16":
			case "ic-17":
			case "ic-18":
			case "ic-19":
			case "ic-20":
			case "ic-21":
			case "ic-22":
			case "ic-23":
			case "ic-24":
			case "ic-25":
			case "ic-26":
			case "ic-27":
				$("#mensagem").show().html(msgPrazo);	
				$(".fac").css("min-height", "330px");
				$("#form-toggle").attr("disabled", false);
				$("#form-toggle").show().html(formCorretor+textAreaInfoCampanha);
				$("#form-toggle input").attr("disabled", false);
				$(".bt_enviar").css("display", "inline-block");
				$(".bt_voltar").css("display", "inline-block");
				$(".telefones").show();
				$("#telefones-regioes").show();
				break;
		}

		$("input[name='cpf']").mask("000.000.000-00");
		$("input[name='cpf-cliente']").mask("000.000.000-00");
		$("input[name*='tel']").mask("(99) 99999-9999");
		$("input[name='celular']").mask("(99) 99999-9999");
	});
});

$(".telregiao").on("change", function() {

		$(".alagoas").hide();
		$(".amazonas").hide();
		$(".bahia").hide();
		$(".ceara").hide();
		$(".espiritosanto").hide();
		$(".maranhao").hide();
		$(".minasgerais").hide();
		$(".para").hide();
		$(".paraiba").hide();
		$(".pernambuco").hide();
		$(".piaui").hide();
		$(".riograndedonorte").hide();
		$(".saopaulo").hide();
		$(".sergipe").hide();


		var telregiao = $(this).val();

		switch(telregiao) {
			case "alagoas":
				$(".alagoas").show();
				break;
		}
	});

$(document).ready(function() {
         $(window).scroll(function () {
              if ($(this).scrollTop() > 400) {
                  $('#mensagem').addClass('fixed');
              } else {
                  $('#mensagem').removeClass('fixed');
              }
        });
 });


</script>

<br />

<div class="telefones">
<ul class="contato-telefones">

<li class="col1">
<strong>Central de Atendimento:</strong><br>
De segunda a sexta, das 8h às 20h.
</li>

<li class="col2 tel-choose">
<strong>Para chamadas de telefones fixos e celulares</strong><br>
<select id="telefonesregioes" name="telefonesregioes" class="telregiao">
		<option selected="selected">Escolha o seu estado</option>
		<option value="alagoas">Alagoas - Maceió - (82) 3029-5949</option>
		<option value="amazonas">Amazonas - Manaus - (92) 3042-1899</option>
		<option value="bahia">Bahia - Salvador - (71) 4062-5949</option>
		<option value="ceara">Ceará - Fortaleza - (85) 4062-5949</option>
		<option value="espiritosanto">Espírito Santo - Vitória - (27) 4062-5949</option>
		<option value="maranhao">Maranhão - São Luís - (98) 3042-1993</option>
		<option value="minasgerais">Minas Gerais - Belo Horizonte - (31) 4062-5949</option>
		<option value="para">Pará - Belém - (91) 4042-1720</option>
		<option value="paraiba">Paraíba - João Pessoa - (83) 4062-5949</option>
		<option value="pernambuco">Pernambuco - Recife - (81) 4062-5949</option>
		<option value="piaui">Piauí - Teresina - (86) 3142-1828</option>
		<option value="riograndedonorte">Rio Grande do Norte - Natal - (84) 3034-5949</option>
		<option value="saopaulo">São Paulo - São Paulo - (11) 4062-5949 </option>
		<option value="sergipe">Sergipe - Aracajú - (79) 3028-5949</option>
</select>

<br><br>

<strong>Para chamadas de telefones fixos de qualquer região<br>
<h3>0800 608 1020</h3>
</strong>

</li>

</ul>
</div>


	</div><!-- .entry-content -->
</article><!-- #post-## -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer();
