## Fale Conosco | Affix
---

**Descrição geral:** Formulário contendo diversas opções de contato para clientes, corretores, assim
como visitantes que queiram se tornar clientes ou vendedores Affix.

**Descrição técnica:** A cada opção selecionada um código é enviado ao arquivo ura.php que envia
o mesmo á um webservice que torna as opções selecionadas pelos visitantes do site dados estatísticos.
As opções que possuem formulário são enviadas utilizando-se a biblioteca PHP Mailer. O FAC possui adaptação para o ambiente Wordpress onde é inserido no site em produção.

**[Acessar o FAC](http://www.affixbeneficios.com.br/novofac-teste/)**


*Arte Digital Internet*
