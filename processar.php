<?php

if(isset($_POST["submit"]) and !empty($_POST["submit"])) {

	require_once("class/class.phpmailer.php");
	require_once("class/class.smtp.php");
	require_once("includes/functions.php");

	$arquivos = array();

	if(isset($_FILES["arquivos"])){

		foreach($_FILES["arquivos"]["tmp_name"] as $key => $arquivo_path) {

			$arquivo_info = pathinfo($_FILES["arquivos"]["name"][$key]);

			$extensions = array("doc", "docx", "pdf", "jpg", "png", "gif");

			if(in_array($arquivo_info["extension"], $extensions)) {

				$new_path = "uploads/".uniqid().'.'.$arquivo_info['extension'];
				move_uploaded_file($arquivo_path, $new_path);
				$arquivos[]	= sprintf(
					'<li><a href="%s">%s</a></li>',
					'http://'.$_SERVER["SERVER_NAME"].'/wp-content/themes/odin-master/'.$new_path,
					$_FILES["arquivos"]["name"][$key]
				);
			}
		}
	}

	$msgFormPadrao = @"<li><b>Nome: </b>{$_POST['nome']}</li><li><b>CPF Titular: </b>{$_POST['cpf']}</li><li><b>Telefone: </b>{$_POST['telefone']}</li><li><b>E-mail: </b>{$_POST['email']}</li>";

	// O POST cp-opcao recebe conforme está no formulário valores do tipo cp-[numero]
	$estado = array(
		"cp-1" => array(
			"estado" => "Acre",
			"email" => ""
		),
		"cp-2" => array(
			"estado" => "Alagoas",
			"email" => "comercial.AL@affixbeneficios.com.br"
		),
		"cp-3" => array(
			"estado" => "Amapá",
			"email" => ""
		),
		"cp-4" => array(
			"estado" => "Amazonas",
			"email" => "comercial.AM@affixbeneficios.com.br"
		),
		"cp-5" => array(
			"estado" => "Bahia",
			"email" => "comercial.BA@affixbeneficios.com.br"
		),
		"cp-6" => array(
			"estado" => "Ceará",
			"email" => "comercial.CE@affixbeneficios.com.br"
		),
		"cp-7" => array(
			"estado" => "Distrito Federal",
			"email" => "comercial.DF@affixbeneficios.com.br"
		),
		"cp-8" => array(
			"estado" => "Espírito Santo",
			"email" => "comercial.ES@affixbeneficios.com.br"
		),
		"cp-9" => array(
			"estado" => "Goiás",
			"email" => "comercial.GO@affixbeneficios.com.br"
		),
		"cp-10" => array(
			"estado" => "Maranhão",
			"email" => "comercial.MA@affixbeneficios.com.br"
		),
		"cp-11" => array(
			"estado" => "Mato Grosso",
			"email" => "comercial.MT@affixbeneficios.com.br"
		),
		"cp-12" => array(
			"estado" => "Mato Grosso do Sul",
			"email" => "comercial.MS@affixbeneficios.com.br"
		),
		"cp-13" => array(
			"estado" => "Minas Gerais",
			"email" => "comercial.MG@affixbeneficios.com.br"
		),
		"cp-14" => array(
			"estado" => "Pará",
			"email" => "comercial.PA@affixbeneficios.com.br"
		),
		"cp-15" => array(
			"estado" => "Paraíba",
			"email" => "comercial.PB@affixbeneficios.com.br"
		),
		"cp-16" => array(
			"estado" => "Paraná",
			"email" => ""
		),
		"cp-17" => array(
			"estado" => "Pernambuco",
			"email" => "comercial.PE@affixbeneficios.com.br"
		),
		"cp-18" => array(
			"estado" => "Piauí",
			"email" => ""
		),
		"cp-19" => array(
			"estado" => "Rio de Janeiro",
			"email" => "comercial.RJ@affixbeneficios.com.br"
		),
		"cp-20" => array(
			"estado" => "Rio Grande do Norte",
			"email" => "comercial.RN@affixbeneficios.com.br"
		),
		"cp-21" => array(
			"estado" => "Rio Grande do Sul",
			"email" => ""
		),
		"cp-22" => array(
			"estado" => "Rondônia",
			"email" => ""
		),
		"cp-23" => array(
			"estado" => "Roraima",
			"email" => ""
		),
		"cp-24" => array(
			"estado" => "Santa Catarina",
			"email" => ""
		),
		"cp-25" => array(
			"estado" => "São Paulo",
			"email" => "comercial.SP@affixbeneficios.com.br"
		),
		"cp-26" => array(
			"estado" => "Sergipe",
			"email" => "comercial.SE@affixbeneficios.com.br"
		),
		"cp-27" => array(
			"estado" => "Tocantins",
			"email" => ""
		),
		"ic-1" => array(
			"estado" => "Acre",
			"email" => ""
		),
		"ic-2" => array(
			"estado" => "Alagoas",
			"email" => "comercial.AL@affixbeneficios.com.br"
		),
		"ic-3" => array(
			"estado" => "Amapá",
			"email" => ""
		),
		"ic-4" => array(
			"estado" => "Amazonas",
			"email" => "comercial.AM@affixbeneficios.com.br"
		),
		"ic-5" => array(
			"estado" => "Bahia",
			"email" => "comercial.BA@affixbeneficios.com.br"
		),
		"ic-6" => array(
			"estado" => "Ceará",
			"email" => "comercial.CE@affixbeneficios.com.br"
		),
		"ic-7" => array(
			"estado" => "Distrito Federal",
			"email" => "comercial.DF@affixbeneficios.com.br"
		),
		"ic-8" => array(
			"estado" => "Espírito Santo",
			"email" => "comercial.ES@affixbeneficios.com.br"
		),
		"ic-9" => array(
			"estado" => "Goiás",
			"email" => "comercial.GO@affixbeneficios.com.br"
		),
		"ic-10" => array(
			"estado" => "Maranhão",
			"email" => "comercial.MA@affixbeneficios.com.br"
		),
		"ic-11" => array(
			"estado" => "Mato Grosso",
			"email" => "comercial.MT@affixbeneficios.com.br"
		),
		"ic-12" => array(
			"estado" => "Mato Grosso do Sul",
			"email" => "comercial.MS@affixbeneficios.com.br"
		),
		"ic-13" => array(
			"estado" => "Minas Gerais",
			"email" => "comercial.MG@affixbeneficios.com.br"
		),
		"ic-14" => array(
			"estado" => "Pará",
			"email" => "comercial.PA@affixbeneficios.com.br"
		),
		"ic-15" => array(
			"estado" => "Paraíba",
			"email" => "comercial.PB@affixbeneficios.com.br"
		),
		"ic-16" => array(
			"estado" => "Paraná",
			"email" => ""
		),
		"ic-17" => array(
			"estado" => "Pernambuco",
			"email" => "comercial.PE@affixbeneficios.com.br"
		),
		"ic-18" => array(
			"estado" => "Piauí",
			"email" => ""
		),
		"ic-19" => array(
			"estado" => "Rio de Janeiro",
			"email" => "comercial.RJ@affixbeneficios.com.br"
		),
		"ic-20" => array(
			"estado" => "Rio Grande do Norte",
			"email" => "comercial.RN@affixbeneficios.com.br"
		),
		"ic-21" => array(
			"estado" => "Rio Grande do Sul",
			"email" => ""
		),
		"ic-22" => array(
			"estado" => "Rondônia",
			"email" => ""
		),
		"ic-23" => array(
			"estado" => "Roraima",
			"email" => ""
		),
		"ic-24" => array(
			"estado" => "Santa Catarina",
			"email" => ""
		),
		"ic-25" => array(
			"estado" => "São Paulo",
			"email" => "comercial.SP@affixbeneficios.com.br"
		),
		"ic-26" => array(
			"estado" => "Sergipe",
			"email" => "comercial.SE@affixbeneficios.com.br"
		),
		"ic-27" => array(
			"estado" => "Tocantins",
			"email" => ""
		)
	);
	
	// Gera o numero para identificação de cada email
	$num_seq = file_get_contents("./sequence.txt");
	$num_seq = ltrim($num_seq, '0');
	$num_seq++;
	$num_seq = str_pad($num_seq,6,"0",STR_PAD_LEFT);
	file_put_contents("./sequence.txt", $num_seq);

	$msgFormContratar = @"<li><b>Nome completo: </b>{$_POST['nome']}</li><li><b>Telefone fixo: </b>{$_POST['telfixo']}</li><li><b>Celular com DDD: </b>{$_POST['celular']}</li><li><b>Cidade: </b>{$_POST['cidade']}</li><li><b>Estado: </b>{$estado[$_POST['cp-opcao']]['estado']}</li><li><b>E-mail: </b>{$_POST['email']}</li>";

	// todo: ajustar conforme necessário ou reutilizar o que está acima
	$msgFormVendedor = @"<li><b>Nome completo: </b>{$_POST['nome']}</li><li><b>Telefone fixo: </b>{$_POST['telfixo']}</li><li><b>Celular com DDD: </b>{$_POST['celular']}</li><li><b>Cidade: </b>{$_POST['cidade']}</li><li><b>Estado: </b>{$estado[$_POST['cp-opcao']]['estado']}</li><li><b>E-mail: </b>{$_POST['email']}</li>";

	$msgFormCorretor = @"<li><b>Nome completo: </b>{$_POST['nome']}</li><li><b>CPF: </b>{$_POST['cpf']}</li><li><b>Telefone: </b>{$_POST['telefone']}</li><li><b>E-mail: </b>{$_POST['email']}</li><li><b>Nome da corretora: </b>{$_POST['corretora']}</li><li><b>CPF do cliente: </b>{$_POST['cpf-cliente']}</li>";

	$msgFormInfoCampanha = @"<li><b>Nome completo: </b>{$_POST['nome']}</li><li><b>CPF: </b>{$_POST['cpf']}</li><li><b>Telefone: </b>{$_POST['telefone']}</li><li><b>E-mail: </b>{$_POST['email']}</li><li><b>Nome da corretora: </b>{$_POST['corretora']}</li>";

	$textAreaPadrao = @"<li><b>Comentários: </b>{$_POST['comentarios']}</li>";

	$textAreaCorretor = @"<li><b>Mensagem do cliente: </b>{$_POST['msg-cliente']}</li>";

	$textAreaInfoCampanha = @"<li><b>Mensagem: </b>{$_POST['msg-cliente']}</li>";

	$atualizacaoRG = "AFFIX Benefícios #".$num_seq." | Atualização de dados > Nome, RG, CPF e data de nascimento";
	$pagamentosDuplicidade = "AFFIX Benefícios #".$num_seq." | Pagamentos > Pagamento efetuado em duplicidade";
	$pagamentosConfirmar = "AFFIX Benefícios #".$num_seq." | Pagamentos > Confirmar pagamento realizado";
	$alteracaoAcomodacao = "AFFIX Benefícios #".$num_seq." | Alteração de acomodação";
	$cartaPermanencia = "AFFIX Benefícios #".$num_seq." | Carta de Permanência";
	$inclusaoRecemNascido = "AFFIX Benefícios #".$num_seq." | Inclusão de recém-nascido e recém casado";
	$cartaAutorizacao = "AFFIX Benefícios #".$num_seq." | Carta de Autorização - Unimed Rio Grande do Norte e Unimed Norte/Nordeste";
	$elogio = "AFFIX Benefícios #".$num_seq." | Elogio";
	$reclamacaoFaltaInformacao = "AFFIX Benefícios #".$num_seq." | Reclamação > Falta de informação na contratação";
	$reclamacaoRecusaAtendimento = "AFFIX Benefícios #".$num_seq." | Reclamação > Recusa de atendimento";
	$reclamacaoReajusteValor = "AFFIX Benefícios #".$num_seq." | Reclamação > Reajuste de valor";
	$contratarPlano = "AFFIX Benefícios #".$num_seq." | Quero contratar um plano de saúde > ".@$estado[$_POST["cp-opcao"]]["estado"];
	$serVendedor = "AFFIX Benefícios #".$num_seq." | Quero ser um vendedor";
	$duvidaPremiacao = "AFFIX Benefícios #".$num_seq." | Dúvida referente premiação";
	$cartaoPremiacaoSenha = "AFFIX Benefícios #".$num_seq." | Cartão Premiação > Senha";
	$cartaoPremiacaoBloqueio = "AFFIX Benefícios #".$num_seq." | Cartão Premiação > Bloqueio";
	$cartaoPremiacaoVia = "AFFIX Benefícios #".$num_seq." | Cartão Premiação > 2ª Via de cartão";
	$informacaoCampanha = "AFFIX Benefícios #".$num_seq." | Informação de campanha > ".@$estado[$_POST["corretor-informacao-campanha"]]["estado"];
	$informacaoBeneficiario = "AFFIX Benefícios #".$num_seq." | Informação de beneficiário";

	$mailAtendimento = "atendimento@affixbeneficios.com.br";
	$mailFinanceiro = "premiacaosp@affixbeneficios.com.br";
	$mailComercial1 = "comercial1@affixbeneficios.com.br";
	$mailComercial2 = "comercial2@affixbeneficios.com.br";

	$msg = array(
		"atualizacao-dados" => array(
			"rg" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Atualização de Dados > Nome, RG, CPF e data de nascimento.</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul><p style='font-family:arial,sans-serif;'>Arquivos para download:<ul style='font-family:arial,sans-serif;list-style:none;'>".@implode('', $arquivos)."</ul></p>"
		),
		"pagamentos" => array(
			"duplicidade-pagamento" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Pagamentos > Pagamento efetuado em duplicidade.</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul><p style='font-family:arial,sans-serif;'>Arquivos para download:<ul style='font-family:arial,sans-serif;list-style:none;'>".@implode('', $arquivos)."</ul></p>",
			"confirmar-pagamento" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Pagamentos > Confirmar pagamento realizado.</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul><p style='font-family:arial,sans-serif;'>Arquivos para download:<ul style='font-family:arial,sans-serif;list-style:none;'>".@implode('', $arquivos)."</ul></p>"
		),
		"alteracao-acomodacao" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Alteração de acomodação.</p><ul style='font-family:arial,sans-serif;list-style:none;'>{$msgFormPadrao}</ul>",
		"carta-permanencia" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Carta de Permanência</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul>",
		"inclusao-recemnascido" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Inclusão de recém-nascido e recém casado</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul><p style='font-family:arial,sans-serif;'>Arquivos para download:<ul style='font-family:arial,sans-serif;list-style:none;'>".@implode('', $arquivos)."</ul></p>",
		"carta-autorizacao" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Carta de Autorização - Unimed Rio Grande do Norte e Unimed Norte/Nordeste</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul>",
		"elogio" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Elogio</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul>",
		"reclamacao" => array(
			"falta-informacao" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Reclamação > Falta de informação na contratação</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul>",
			"recusa-atendimento" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Reclamação > Recusa de atendimento</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul>",
			"reajuste-valor" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Reclamação > Reajuste de valor</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormPadrao.$textAreaPadrao."</ul>"
		),
		"contratar-plano" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Quero contratar um plano de saúde > ".@$estado[$_POST["cp-opcao"]]["estado"]."</p><ul style='font-family:arial,sans-serif;list-style:none;'>{$msgFormContratar}</ul>",
		"duvida-premiacao" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Dúvida referente premiação.</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormCorretor.$textAreaCorretor."</ul>",
		"cartao-premiacao" => array(
			"senha" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Cartão Premiação > Senha.</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormCorretor.$textAreaCorretor."</ul>",
			"bloqueio" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Cartão Premiação > Bloqueio.</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormCorretor.$textAreaCorretor."</ul>",
			"segundavia-cartao" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Cartão Premiação > 2ª Via de cartão.</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormCorretor.$textAreaCorretor."</ul>"
		),
		"informacao-campanha" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Informação de campanha > ".@$estado[$_POST["corretor-informacao-campanha"]]["estado"]."</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormInfoCampanha.$textAreaInfoCampanha."</ul>",
		"informacao-beneficiario" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Informação do beneficiário.</p><ul style='font-family:arial,sans-serif;list-style:none;'>".$msgFormCorretor.$textAreaCorretor."</ul>",
		"vendedor" => "<img src='http://www.affixbeneficios.com.br/wp-content/themes/odin-master/imagens/topo.png'><h2 style='font-family:arial,sans-serif;color:#0d3077;'>Dados do formulário</h2><p style='font-family:arial,sans-serif;'><b style='color:#009B5D;'>Opção escolhida:</b> Quero ser um vendedor.</p><ul style='font-family:arial,sans-serif;list-style:none;'>{$msgFormVendedor}</ul>"
	);


	switch($_POST["form-type"]) {

		case "padrao":

			$dados = array(
				'data' => date('d/m/Y H:i:s'),
				'nome'  => $_POST['nome'],
				'cpf' => $_POST['cpf'],
				'telefone' => $_POST['telefone'],
				'email' => $_POST['email'],
				'comentarios' => $_POST['comentarios'],
			);

			gravar_excel("csv/padrao.csv", $dados);

			break;

		case "contratar":

			$dados = array(
				'data'  => date('d/m/Y H:i:s'),
				'nome' => $_POST['nome'],
				'telfixo' => $_POST['telfixo'],
				'celular' => $_POST['celular'],
				'cidade' => $_POST['cidade'],
				'estado' => $estado[$_POST['cp-opcao']]['estado'],
				'email' => $_POST['email']
			);

			gravar_excel("csv/contratar.csv", $dados);

			break;

		case "vendedor":

			$dados = array(
				'data'  => date('d/m/Y H:i:s'),
				'nome' => $_POST['nome'],
				'telfixo' => $_POST['telfixo'],
				'celular' => $_POST['celular'],
				'cidade' => $_POST['cidade'],
				'estado' => $estado[$_POST['cp-opcao']]['estado'],
				'email' => $_POST['email']
			);

			gravar_excel("csv/vendedor.csv", $dados);

			break;

		case "corretorcpf":

			$dados = array(
				'data'  => date('d/m/Y H:i:s'),
				'nome' => $_POST['nome'],
				'cpf' => $_POST['cpf'],
				'telefone' => $_POST['telefone'],
				'email' => $_POST['email'],
				'corretora' => $_POST['corretora'],
				'cpf-cliente' => $_POST['cpf-cliente'],
				'msg-cliente' => $_POST['msg-cliente']
			);

			gravar_excel("csv/corretorcpf.csv", $dados);

			break;

		case "corretor":

			$dados = array(
				'data'  => date('d/m/Y H:i:s'),
				'nome' => $_POST['nome'],
				'cpf' => $_POST['cpf'],
				'telefone' => $_POST['telefone'],
				'email' => $_POST['email'],
				'corretora' => $_POST['corretora'],
				'msg-cliente' => $_POST['msg-cliente']
			);

			gravar_excel("csv/corretor.csv", $dados);

			break;
	}



	$mail = new PHPMailer(true);
	$mail->IsSMTP();
	$mail->IsHTML(true);
	$mail->CharSet = "UTF-8";

	try {

		$mail->Host = "exchange.penso.com.br";
		$mail->Port = 25;
		$mail->Username = "atendimento@affixbeneficios.com.br";
		$mail->Password = "Tede@12";

		$mail->SetFrom("atendimento@affixbeneficios.com.br");

		// Evita empilhamento de funções
		$mail->ClearAddresses();
		$mail->ClearReplyTos();
		$mail->ClearBCCs();

		// Primeiro email enviado ao visitante como comprovante
		$mail->AddAddress($_POST["email"], $_POST["nome"]);

		$mail->addBCC("pegatudo@artedigitalinternet.com.br", "Pegatudo FAC Affix");

		if(isset($_POST["cliente-sim"])) {

			if($_POST["cliente-sim"] == "atualizacao-dados") {

				if($_POST["cliente-sim-atualizacao-dados"] == "rg") {
					$mail->Subject = $atualizacaoRG;

					$mail->MsgHTML("<div class='template-email'>{$msg['atualizacao-dados']['rg']}</div>");

					// Envio para o visitante do site
					$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress
					$mail->ClearAddresses();

					// Envio para a AFFIX
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailAtendimento, "Atendimento Affix");
				}	
			} else if($_POST["cliente-sim"] == "pagamentos") {

				if($_POST["cliente-sim-pagamentos"] == "duplicidade-pagamento") {
					$mail->Subject = $pagamentosDuplicidade;
					
					$mail->MsgHTML("<div class='template-email'>{$msg['pagamentos']['duplicidade-pagamento']}</div>");

					// Envio para o visitante do site
					$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress
					$mail->ClearAddresses();

					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailAtendimento, "Atendimento Affix");
				} else
				if($_POST["cliente-sim-pagamentos"] == "confirmar-pagamento") {
					$mail->Subject = $pagamentosConfirmar;
					
					$mail->MsgHTML("<div class='template-email'>{$msg['pagamentos']['confirmar-pagamento']}</div>");

					// Envio para o visitante do site
					$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress
					$mail->ClearAddresses();

					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailAtendimento, "Atendimento Affix");
				}
			} else
			if($_POST["cliente-sim"] == "alteracao-acomodacao") {
				$mail->Subject = $alteracaoAcomodacao;

				$mail->MsgHTML("<div class='template-email'>{$msg['alteracao-acomodacao']}</div>");

				// Envio para o visitante do site
				$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress
				$mail->ClearAddresses();

				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailAtendimento, "Atendimento Affix");
			} else
			if($_POST["cliente-sim"] == "carta-permanencia") {
				$mail->Subject = $cartaPermanencia;

				$mail->MsgHTML("<div class='template-email'>{$msg['carta-permanencia']}</div>");

				// Envio para o visitante do site
				$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress
				$mail->ClearAddresses();

				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailAtendimento, "Atendimento Affix");
			} else
			if($_POST["cliente-sim"] == "inclusao-recemnascido") {
				$mail->Subject = $inclusaoRecemNascido;

				$mail->MsgHTML("<div class='template-email'>{$msg['inclusao-recemnascido']}</div>");

				// Envio para o visitante do site
				$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress
				$mail->ClearAddresses();

				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailAtendimento, "Atendimento Affix");
			} else
			if($_POST["cliente-sim"] == "carta-autorizacao") {
				$mail->Subject = $cartaAutorizacao;

				$mail->MsgHTML("<div class='template-email'>{$msg['carta-autorizacao']}</div>");

				// Envio para visitante do site
				$mail->addReplyTo("naoresponda@affixbeneficios.com.br");	
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress
				$mail->ClearAddresses();

				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailAtendimento, "Atendimento Affix");
			} else
			if($_POST["cliente-sim"] == "elogio") {
				$mail->Subject = $elogio;				
				
				$mail->MsgHTML("<div class='template-email'>{$msg['elogio']}</div>");

				// Envio para visitante do site
				$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress
				$mail->ClearAddresses();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailAtendimento, "Atendimento Affix");
			} else
			if($_POST["cliente-sim"] == "reclamacao") {

				if($_POST["cliente-sim-reclamacao"] == "falta-informacao") {
					$mail->Subject = $reclamacaoFaltaInformacao;
					
					$mail->MsgHTML("<div class='template-email'>{$msg['reclamacao']['falta-informacao']}</div>");

					// Envio para visitante do site
					$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress
					$mail->ClearAddresses();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailAtendimento, "Atendimento Affix");
				} else
				if($_POST["cliente-sim-reclamacao"] == "recusa-atendimento") {
					$mail->Subject = $reclamacaoRecusaAtendimento;
						
					$mail->MsgHTML("<div class='template-email'>{$msg['reclamacao']['recusa-atendimento']}</div>");

					// Envio para visitante do site
					$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress
					$mail->ClearAddresses();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailAtendimento, "Atendimento Affix");
				} else
				if($_POST["cliente-sim-reclamacao"] == "reajuste-valor") {
					$mail->Subject = $reclamacaoReajusteValor;
											
					$mail->MsgHTML("<div class='template-email'>{$msg['reclamacao']['reajuste-valor']}</div>");

					// Envio para visitante do site
					$mail->addReplyTo("naoresponda@affixbeneficios.com.br");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress
					$mail->ClearAddresses();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailAtendimento, "Atendimento Affix");
				}
			}
		} else
		if(isset($_POST["cliente-nao"])) {

			$mail->Subject = $contratarPlano;
			// todo: descomentar a linha abaixo dps do teste
			// $mail->MsgHTML("<div class='template-email'>{$msg['contratar-plano']}</div>");
			// todo: limpar testes buscando por [Destinatario: 

			if($_POST["cp-opcao"] == "cp-1") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-2") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.AL]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-3") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
			} else
			if($_POST["cp-opcao"] == "cp-4") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.AM]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-5") {
		
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.BA]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
			} else
			if($_POST["cp-opcao"] == "cp-6") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.CE]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
			} else
			if($_POST["cp-opcao"] == "cp-7") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.DF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
			} else
			if($_POST["cp-opcao"] == "cp-8") {
			
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.ES]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-9") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.GO]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-10") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.MA]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-11") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.MT]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-12") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.MS]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-13") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.MG]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-14") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.PA]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-15") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.PB]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
			
			} else
			if($_POST["cp-opcao"] == "cp-16") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
	
			} else
			if($_POST["cp-opcao"] == "cp-17") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.PE]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-18") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				
			} else
			if($_POST["cp-opcao"] == "cp-19") {
				
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.RJ]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				
			} else
			if($_POST["cp-opcao"] == "cp-20") {
			
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.RN]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-21") {
			
				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				// $mail->AddAddress($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
		
			} else
			if($_POST["cp-opcao"] == "cp-22") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				
			} else
			if($_POST["cp-opcao"] == "cp-23") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				
			} else
			if($_POST["cp-opcao"] == "cp-24") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");				
				// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
					
			} else
			if($_POST["cp-opcao"] == "cp-25") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.SP]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

			} else
			if($_POST["cp-opcao"] == "cp-26") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.SE]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
			
			} else
			if($_POST["cp-opcao"] == "cp-27") {

				$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['contratar-plano']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();
				
				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
				// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				
			}
		} else
		if(isset($_POST["corretor"])) {
			if($_POST["corretor"] == "duvida-premiacao") {

				$mail->SetFrom("premiacaosp@affixbeneficios.com.br");

				$mail->Subject = $duvidaPremiacao;
														
				$mail->MsgHTML("<div class='template-email'>{$msg['duvida-premiacao']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailFinanceiro, "Financeiro Affix");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();

				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailFinanceiro, "Financeiro Affix");
			} else
			if($_POST["corretor"] == "cartao-premiacao") {

				if($_POST["corretor-cartao-premiacao"] == "senha") {

					$mail->SetFrom("premiacaosp@affixbeneficios.com.br");

					$mail->Subject = $cartaoPremiacaoSenha;
																
					$mail->MsgHTML("<div class='template-email'>{$msg['cartao-premiacao']['senha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailFinanceiro, "Financeiro Affix");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailFinanceiro, "Financeiro Affix");
				} else
				if($_POST["corretor-cartao-premiacao"] == "bloqueio") {

					$mail->SetFrom("premiacaosp@affixbeneficios.com.br");

					$mail->Subject = $cartaoPremiacaoBloqueio;
																
					$mail->MsgHTML("<div class='template-email'>{$msg['cartao-premiacao']['bloqueio']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailFinanceiro, "Financeiro Affix");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailFinanceiro, "Financeiro Affix");
				} else
				if($_POST["corretor-cartao-premiacao"] == "segundavia-cartao") {

					$mail->SetFrom("premiacaosp@affixbeneficios.com.br");

					$mail->Subject = $cartaoPremiacaoVia;
																
					$mail->MsgHTML("<div class='template-email'>{$msg['cartao-premiacao']['segundavia-cartao']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailFinanceiro, "Financeiro Affix");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailFinanceiro, "Financeiro Affix");
				}
			} else
			if($_POST["corretor"] == "informacao-campanha") {
				
				$mail->Subject = $informacaoCampanha;
				// todo: descomentar a linha abaixo dps do teste
				// $mail->MsgHTML("<div class='template-email'>{$msg['informacao-campanha']}</div>");
				// todo: limpar testes buscando por [Destinatario:

				if($_POST["corretor-informacao-campanha"] == "ic-1") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-2") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.AL]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-3") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-4") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.AM]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-5") {
			
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.BA]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-6") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.CE]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-7") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1
					Copia: comercial.DF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-8") {
				
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.ES]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-9") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.GO]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-10") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.MA]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-11") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.MT]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-12") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1
					Copia: comercial.MS]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-13") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1
					Copia: comercial.MG]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-14") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.PA]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-15") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1
					Copia: comercial.PB]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["corretor-informacao-campanha"] == "ic-16") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-17") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.PE]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["corretor-informacao-campanha"] == "ic-18") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-19") {
					
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.RJ]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-20") {
				
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.RN]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["corretor-informacao-campanha"] == "ic-21") {
				
					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");
					
				} else
				if($_POST["corretor-informacao-campanha"] == "ic-22") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-23") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-24") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-25") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial1 
					Copia: comercial.SP]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["corretor-informacao-campanha"] == "ic-26") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: comercial.SE]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");

					} else
				if($_POST["corretor-informacao-campanha"] == "ic-27") {

					$mail->MsgHTML("<div class='template-email'>[Destinatario: comercial2 
					Copia: sem email UF]{$msg['informacao-campanha']}</div>");

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["corretor-informacao-campanha"]]["email"], "Affix - Área Comercial");
				}

			} else
			if($_POST["corretor"] == "informacao-beneficiario") {
				$mail->Subject = $informacaoBeneficiario;
															
				$mail->MsgHTML("<div class='template-email'>{$msg['informacao-beneficiario']}</div>");

				$mail->ClearReplyTos();
				$mail->addReplyTo($mailAtendimento, "Atendimento Affix");
				$mail->Send();

				// Evita empilhamento do efeito de AddAddress e AddReply.
				$mail->ClearAddresses();
				$mail->ClearReplyTos();

				$mail->addReplyTo($_POST["email"], $_POST["nome"]);
				$mail->AddAddress($mailAtendimento, "Atendimento Affix");
			}
		} else
		if(isset($_POST["cliente-vendedor"])) {

			if($_POST["cliente-vendedor"] == "vendedor") {

				$mail->Subject = $serVendedor;
				$mail->MsgHTML("<div class='template-email'>{$msg['vendedor']}</div>");

				if($_POST["cp-opcao"] == "cp-1") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["cp-opcao"] == "cp-2") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-3") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-4") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["cp-opcao"] == "cp-5") {
			
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-6") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["cp-opcao"] == "cp-7") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["cp-opcao"] == "cp-8") {
				
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-9") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
					
				} else
				if($_POST["cp-opcao"] == "cp-10") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-11") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-12") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-13") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-14") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-15") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
					
				} else
				if($_POST["cp-opcao"] == "cp-16") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");					
				} else
				if($_POST["cp-opcao"] == "cp-17") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
					
				} else
				if($_POST["cp-opcao"] == "cp-18") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
					
				} else
				if($_POST["cp-opcao"] == "cp-19") {
					
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
						
				} else
				if($_POST["cp-opcao"] == "cp-20") {
				
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
					
				} else
				if($_POST["cp-opcao"] == "cp-21") {
				
					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["cp-opcao"] == "cp-22") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-23") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");

				} else
				if($_POST["cp-opcao"] == "cp-24") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				} else
				if($_POST["cp-opcao"] == "cp-25") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial1, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial1, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				
				} else
				if($_POST["cp-opcao"] == "cp-26") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");
					$mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
					
				} else
				if($_POST["cp-opcao"] == "cp-27") {

					$mail->ClearReplyTos();
					$mail->addReplyTo($mailComercial2, "Affix - Área Comercial");
					$mail->Send();

					// Evita empilhamento do efeito de AddAddress e AddReply.
					$mail->ClearAddresses();
					$mail->ClearReplyTos();
					
					$mail->addReplyTo($_POST["email"], $_POST["nome"]);
					$mail->AddAddress($mailComercial2, "Affix - Área Comercial");				
					// $mail->AddBCC($estado[$_POST["cp-opcao"]]["email"], "Affix - Área Comercial");
				}
			}
		}

		// Envio para AFFIX
		$mail->Send();

		header("Location: http://www.affixbeneficios.com.br/novofac-teste/?envio=ok");

	} catch (Exception $e) {

		echo $e->getMessage();

	}

} else {
	header("Location: http://www.affixbeneficios.com.br/novofac-teste/");
}
