<?php

function gravar_excel($arquivo, $dados)
{
	$handle = fopen($arquivo, "a");

	// ser o arquivo estiver vazio ele inclui o cabeçalho
	if (!filesize($arquivo)) {
		fputcsv($handle, array_keys($dados));
	}

	fputcsv($handle, array_values($dados));
	fclose($handle);
}
